public interface Relation{

	public boolean isGreater(object a,object b);
	public boolean isLess(object a,object b);
	public boolean isEqual(object a,object b);
}