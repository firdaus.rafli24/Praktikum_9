public class HighSchoolStudent extends StudentRecord{

	private double computerGrade;
	
	
	public HighSchoolStudent(String name){
		
		setName(name);
	}
		
	public void setComGrade(double Grade){

		this.computerGrade = Grade;
	}

	public double getComGrade(){

		return computerGrade;
	}

	public void print(double mGrade,double eGrade,double sGrade){

		System.out.println("Name: "+getName());
		System.out.println("Math Grade: "+getMathGrade());
		System.out.println("English Grade: "+getEngGrade());
		System.out.println("Science Grade: "+getSciGrade());
		System.out.println("Computer Grade: "+getComGrade());
		System.out.println();

	}

	public static void main(String [] args){

		HighSchoolStudent annaRecord = new HighSchoolStudent("Anna");
			
		annaRecord.setMathGrade(88);
		annaRecord.setEngGrade(90);
		annaRecord.setSciGrade(88);
		annaRecord.setComGrade(95);
		
		annaRecord.print(annaRecord.getEngGrade(),annaRecord.getMathGrade(),annaRecord.getSciGrade());
		
	
	
	}		
}