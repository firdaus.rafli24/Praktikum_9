public class Baju extends Pakaian{

	public char kodeWarna = 'U';

	public void cetakInformasiBaju(){

		System.out.println("ID Baju: "+getID());
		System.out.println("Keterangan: "+getKeterangan());
		System.out.println("Kode Warna: "+kodeWarna);
		System.out.println("Harga Baju: "+getHarga());
		System.out.println("Jumlah Stok: "+getJmlStock());
	}
}